//
//  AppDelegate.h
//  CustomCalendar
//
//  Created by MAPPS MAC on 29/08/16.
//  Copyright © 2016 Aditi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

